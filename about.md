---
title: About
# permalink: /about/
layout: post
---

Hi, I am Shreyas. Depending on you, I am from either from Thane, Mumbai, Pune, Maharashtra, India, Asia, Northen Hemisphere, Earth, Solar System, the Milky Way or the Virgo Supercluster (last 4 are especially there for the Aliens looking for my blog!)

<!-- I am a master student with major in Embedded Systems and minor in Innovation and Entreprenurship. I like all tech in general, but specifically Robotics, Autonomous Vehicles and IoT. Ich verstehe ein bisschen Deutsch, そしてちっと日本語も！ I am up for travelling and hiking, any time of the year!

Studying in two primer universities in Europe has undoubtedly propelled my passion for two things: Tech and Travel. With very cheap and efficient transportation (I once booked a RyanAir Flight for  € 3.41!), and a whole continent full of tourist destinations, Europe is heaven for anyone who wants to travel. Similarly, when 8 out of the 10 most innovative nations are from Europe, Technological innovation is THE mantra taught in the universities. But I do not want to limit my experineces to myself, after all, sharing is the best way of improving the knowledge. So sit back,relax and join me on my quest to explore the tech and travel of Europe. -->

I used to write on [medium](https://medium.com/@elementaryshr/), but from now on, this is the new home for all my blogs. I will be writing mostly in English and in my native language: Marathi, but I hope someday I'll be able to write a blog in German! 

If you are interested about my work or professional life (I am talking to you, recruiters!), head over to my [main website](https://shreyasgokhale.com/).

*© Shreyas Gokhale, 2021. Unauthorized use and/or duplication of this material without express and written permission from this site’s author and/or owner is strictly prohibited. Excerpts and links may be used, provided that full and clear credit is given with appropriate and specific direction to the original content. All rights reserved.*

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
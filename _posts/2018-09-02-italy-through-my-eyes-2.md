---
title: "Italy Through My Eyes, Part 2"
permalink: /travelogues/:title/
excerpt: "My Travels in Italy: Trieste and Elettra Synchrotrone"
classes: wide
layout: post
header:
  image: /assets/images/travelogue/trieste.jpg
  caption: "Trieste, Italy | ©Shreyas Gokhale"
  teaser: /assets/images/travelogue/trieste.jpg

---

## ***Tryst with Trieste***


![Port of Trieste](https://cdn-images-1.medium.com/max/5664/1*97WaEjQpq3KVDw1sDt70oQ.jpeg)

The first thing I did after getting an acceptance for a hackathon in Trieste was to google what the heck Trieste is! Once a major port town and now an abode to one of the most important synchrotron in the world, it is certainly a destination neglected by many for their Europe trip. But, believe me, if you don’t want to miss grandiose architecture and amazing landscapes, you should definitely plan to visit this hidden gem.

This Italy’s northeastern province is a tale in itself. Sandwiched between the Adriatic sea and Slovenia, it has a rich history and equally beautiful landscapes. Being a port town, Trieste was an amalgamation of different nationalities: Italian, Slovenian, Austrian, Croatian and Greeks. From Habsburg monarchs to Fascist occupation to being an independent state and then finally settling down with Italy, Trieste has changed its flags quite a lot of times.

![](https://cdn-images-1.medium.com/max/6048/1*DD1XA0Cy29R_zEJLjoBvcQ.jpeg)

![Eastern European influence on the architecture](https://cdn-images-1.medium.com/max/6048/1*Vaff0PaLo02jPjJo7hiJjg.jpeg)

My train arrived at the station pretty late in the night. The hotel booked for me was not in Trieste but in Basovizza, an outskirt town which is closer to Slovenia than Trieste. Checking google maps and rummaging through the information, I barely hopped on the bus. Then, I was confronted with my first horror: Bus tickets are not sold on the bus but at the train station or tobacco shops. With nothing open in sight and no chance to get another bus, I stood in despair thinking about what to do. Miraculously, a lady understood my condition and tried to help me with her broken English. Giving me instructions where to get off the bus, she kindly offered her extra ticket. Over my trip, I have witnessed the friendliness of Italians a multiple times. Even if they don’t know English, they will try to construct broken sentences to give you directions. But Trieste was just the next level. Over the next three days, I was helped in numerous ways by unknown strangers, to whom I can’t thank enough.

I alighted the bus at a deserted crossroad, my hotel nowhere to be seen. I walked up to the only house which seemed awake and suddenly there it was! A small yet elegant hotel with a beautiful courtyard. I received a warm welcome from the hosts and after a quick shower, I slumbered in a bed.

![](https://cdn-images-1.medium.com/max/8064/1*EusiRiT-w-PSigX34ObWYw.jpeg)

![](https://cdn-images-1.medium.com/max/8064/1*7V99r8fxBdBMi9QHVsDDOw.jpeg)

![](https://cdn-images-1.medium.com/max/8064/1*iLpDgZ7h6B6RSyOBPFemXg.jpeg)

![Basovizza](https://cdn-images-1.medium.com/max/6048/1*2cx7MVr6P4a6qnn_xO5hpQ.jpeg)

The next morning, I was greeted with a scenic view of a typical Italian countryside. After a quick morning walk, I was bewitched to the extent that I almost forgot the reason why I was here: AttractYoung Hackathon organized by ATTRACT and CERN IdeaSquare.

During a sumptuous breakfast, I met my fellow hackers. It felt like the whole of Europe was here. One was pursuing masters in Material Science from Finland while another friend was learning chemistry in Slovenia. We had to come up with innovative solutions for societal challenges like world hunger, and the best one wins the trip to CERN! We were teamed up based on our topic and were assigned a coach which was an expert in that industry. The AREA innovation park was our proving ground for the next two days. Intensive brainstorming on ideas with people from different backgrounds, nationalities and cultures with lip-smacking pasta and desserts to complement it, a hackathon could not be better.

![Italian Desserts](https://cdn-images-1.medium.com/max/7498/1*kVhcfZz1SPviHSccY2Ifmw.jpeg)

Although our group did not win, we sure learned a lot! After the hackathon, we visited the Elettra Synchrotron facility. In layman’s term, a distant cousin of the big brother in Geneva (Yes, the Large Hadron Collider). The facility is operated 24x7 and experiments are performed by scientists all over the world. This research can be used in pretty much everything: From making a stronger material using nanoscience to [https://www.elettra.trieste.it/science/top-stories/the-teeth-of-a-prehistoric-fetus-give-us-information-about-the-last-months-of-a-mother-and-child-who-lived-27-000-years-bp.html](https://www.elettra.trieste.it/science/top-stories/the-teeth-of-a-prehistoric-fetus-give-us-information-about-the-last-months-of-a-mother-and-child-who-lived-27-000-years-bp.html).

![Elettra Synchrotron](https://cdn-images-1.medium.com/max/8064/1*Pg4NfXdusPMFn2q9sHt5Lw.jpeg)

My schedule got over by 5 in the evening and me and Christian, my Italian friend from the hackathon decided to explore the city. Situated right between the green hills and blue sea, the port of Trieste is inexplicably beautiful! With Roman, Germanic and East European influence, architecture in Trieste offers a unique relic of its past. The stone pier, Molo Audace, offers riveting views during the sunset: of the sea as well as the city. Piazza Unità d’Italia is spectacular enough during the day, but complementing the sunset, it reaches a whole new level!

![](https://cdn-images-1.medium.com/max/8064/1*qjbHmvHV-1n-TbcnnsEXng.jpeg)

![](https://cdn-images-1.medium.com/max/8064/1*Ws6YYaQW7oxN3C_H3N7-zA.jpeg)

![](https://cdn-images-1.medium.com/max/8064/1*XgW8Rn0UhfD4DadQ4EEfMA.jpeg)

![](https://cdn-images-1.medium.com/max/8064/1*C-PEyO2gRgyJKnksxeijCw.jpeg)

As the sun goes down, Trieste starts bustling with people shopping, eating and drinking! Here, I had the best pizza and ice cream in my life, hands down! It was a typical Neapolitan pizzeria which baked in the wood fire oven. The pizza was thin crust with delicious vegetables and fresh cream, and it melted in the mouth faster than an ice cream. Even after eating a whole 8", I was still craving for more! Don’t even bother asking me the name of the pizza; every pizzeria comes up with their own name and way to make it! (Here it was kings and queens).

![The Best Pizza EVER!!!](https://cdn-images-1.medium.com/max/8064/1*hZLghsJkndN4tfyORBdNhQ.jpeg)

My flight back was scheduled at 8 AM from the Treviso airport, so I had to catch the first train in the morning. I bid a farewell to my friends and returned to my hotel. As the train pulled out of the station, the dawn was slowly taking over the glimmering night light of Trieste. My trip was over, but the journey was just getting started.

![](https://cdn-images-1.medium.com/max/8064/1*O6U8XgEg3bt2z5SdTufCPA.jpeg)

If procrastination is a disease, it’s probably embedded in my DNA. Generally, people procrastinate the things they hate to do, I procrastinate everything! I did this trip back in April but I was only able to write about it now. I toured many places meanwhile and unfortunately writing was the second priority. I will try to the next one a bit sooner (than the next season of Game of Thrones 😅) See you soon!

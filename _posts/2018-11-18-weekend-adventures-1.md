---
title: "The Weekend Adventures, Part 1"
permalink: /travelogues/:title/
excerpt: "My adventures in Swtizerland: Geneva and CERN "
classes: wide
header:
  image: /assets/images/travelogue/geneva.jpg
  caption: "Geneva, Switzerland | ©Shreyas Gokhale"
  teaser: /assets/images/travelogue/geneva.jpg
layout: post

---
### Part 1: Geneva And CERN

There is a very popular phenomenon called the “Butterfly Effect”. The idea being: every decision you take has far-reaching implications in the future, and maybe not just yours! Your present is just one path in the decision tree and it could have been completely different if you haven’t had the coffee, that particular morning! I don’t know exactly what decisions weighed in this, but 2 months ago, I roamed in Geneva, had a tour of CERN, managed to do a challenging alpine trek, and jaunted in Paris. Now, when I look back, it seems impossible how I was able to fit all of this in just one weekend!

![Tucked behind the mountain!](https://cdn-images-1.medium.com/max/8064/1*LscRa1DLoyNDcbUJAOqXLQ.jpeg)

The French-speaking town of Geneva ( or Genève!) needs no introduction. Situated right on the border of Switzerland and France, the city is as cosmopolitan as it gets. This abode of the United Nations and numerous important organizations is also famous for its impeccable natural beauty. This Peace Capital is a perfect go-to destination for a weekender.

On a bright, clear Saturday morning, my flight landed on the Cointrin Airport of Geneva. The first thing I noticed is how expensive Switzerland is: Very! If you go to a supermarket to buy something for a quick bite, you will easily spend upwards of 15 CHF (13 €) and still be quite hungry! But don’t worry, I’ve got you covered with some money saving tips! In Geneva, you get a free trip to the city centre with public transport when you arrive by plane. Cool, right? Even cooler: you will get a free public transport pass if you live in any hostel or hotel in the city! (The pass is valid for the entire duration of your stay, and yes, you are welcome!)

![Jet d’eau](https://cdn-images-1.medium.com/max/6048/1*LGssENBHyZB_uF5TPm6-6w.jpeg)

It might sound a bit clichéd, but as a kid, I was super fascinated by mysteries of the universe. Astronomy was my first love and soon enough I was philandering with Physics. Since then, CERN and NASA have been like a Mecca to me. For me, not visiting CERN when visiting Geneva would have been a sin! After ditching my satchel at the hostel, I hopped on tram №18, straight to the CERN. There are some permanent and temporary exhibitions on the premises, but the real gem is the guided tour. And believe me, it’s not easy to get into! First of all, the English version of the tour is only offered a couple times a week. You have to book the slot online, exactly 15 days in advance, at 8.00 in the morning. If you are lucky, then you will be one of the 11 participants which will enjoy a guided tour, inside CERN facilities, accompanied by a profound scientist.

![](https://cdn-images-1.medium.com/max/6048/1*mFF1Jldx8cokzOI8C66Sfw.jpeg)

![](https://cdn-images-1.medium.com/max/8064/1*fII122JN3yn4t1--28Hk1w.jpeg)

We started our tour in the auditorium of the visitor’s centre. We were shown a short yet amazing movie which introduced us briefly to its history and the facility. Did you know that the reason you are able to read this post is due to technology which was developed at the CERN! The LHC (Large Hadron Collider) is one of humanity’s biggest collaborative project (only second to the international space station), and that meant transferring large scientific data, all across the world. Transferring result files was hardly a viable solution. So they asked Tim Berners-Lee to develop CERN’s own framework for transferring files digitally, named ENQUIRE. The rest is history and it became the basis for the World Wide Web as we know now!

[https://youtu.be/h_DmpTLixyQ](https://youtu.be/h_DmpTLixyQ)

We were then separated into two groups. A Dutch scientist (I am so sorry to forget his name) who worked on the superconductors in the LHC was our “Tour Guide”! Giving intriguing and intricate details about how the collider works, he was also happy to answer all our dumb questions. I particularly remember a funny one: Someone asked: “Is there any possibility that the high energy particle collision can create a black hole?” His reply to it was “Even if it did, as everything will get sucked into the black hole, there will be nothing left to worry about!”

![](https://cdn-images-1.medium.com/max/8064/1*idV0Og8_iovzz5clwhkcyQ.jpeg)

![The control room from inside (left) and from outside, with a giant mural of the ATLAS particle detector (right)](https://cdn-images-1.medium.com/max/5576/1*iWvKYvwLVNu6c1IMI4UQ4w.jpeg)

Our next stop was the main control room. For such a big facility, it is a bit of a disappointment. Just a bunch of computers, big screens with important data and researchers drinking coffee. The facility data can be remotely accessed, so no reason to be physically there. However, right upstairs we were shown a mind-blowing 3D movie, explaining how LHC aka giant race track works! The LHC has different phases such as Injection (Injecting particles), Ramp (Accelerating them to 99.999999% of the speed of light), Collision etc. The collision is detected by very sensitive detectors in 4 different parts of LHC depending on the ongoing experiment. It sort of “photographs” the moment of the collision, gathering up to 1 petabyte of raw data per second. Then, the data is processed and shared worldwide for analysis. The whole process is continuous and runs 24x7 except for some part of the year.

 <iframe src="https://medium.com/media/5630e426221074997c0960b906eb412a" frameborder=0></iframe>

The last destination was a visit to the precursor of LHC: the Synchrocyclotron (SC). Built just after the 2nd world war, the SC was not only a sizeable scientific feat but also a step towards the world peace. A machine which brought together the brightest minds in the world to research on, regardless of their nationality, religion or political views. After countless contributions to the major scientific breakthroughs, it was retired in 1990. Now it's a backdrop for showing its construction and history, quite literally! A spectacular laser show on the SC left us awestruck by the achievements of mankind and it also marked the ending of our 2 hr long tour. After spending some more time in the temporary & permanent museums I still wanted to keep exploring this wonderful institution! Sadly, CERN was about to close and before they kick me out, I wanted to purchase some geeky memorabilia.

![My Coffee mug of “The Standard Model” and no, I don’t drink coffee from it! (It's too precious for that!)](https://cdn-images-1.medium.com/max/6048/1*--PnkAyZnvmgu7ZVoyaa-A.jpeg)

![Cityscape of Geneva](https://cdn-images-1.medium.com/max/8064/1*f1M2jykuvM0_pcp0nzejmA.jpeg)

![The famous “Flower Clock”](https://cdn-images-1.medium.com/max/6048/1*jKMFBjpJh7K3wRPxp1MrJw.jpeg)

Later that day, I got a chance to roam around. As they say, it is really the smallest of the big cities. With alps in the south, the town faces Lake Geneva in the north, which creates some spectacular panoramic views. In the middle of the lake is Jet d’Eau, literally a giant jet of water! With a height of 140mtr, it is one of the tallest fountains in the world. Like Eiffel tower, you can see it from everywhere in the city. Walking in the old town and exploring various sights and monuments is really a must experience activity. Migros, one of the biggest (and duty-free!) shopping malls in Geneva, is a shopper’s paradise. They have a section as huge as a mini supermarket dedicated just for the chocolates. If you are into Chanels and Guccis, Switzerland is no less than a ‘Prada’ise!

![Nightscapes of Geneva](https://cdn-images-1.medium.com/max/8064/1*YIJSDJO4bgBJdcXr76jTAg.jpeg)

After a sort of hectic (!) day, I returned to my hostel at 8 PM. Scrolling “Top 10 things to do in Geneva”, I was just planning my second and last day in Geneva. Just then, I noticed “[Rando Saleve](https://www.rando-saleve.net)”, a hiking group which treks every Sunday to a nearby mountain in the Alps. Without considering the fact that I was wearing canvas shoes or I had to travel to Paris the next night, I thought: Why Not!!

**To be continued in Part 2…**

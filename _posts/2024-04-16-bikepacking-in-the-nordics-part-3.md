---
title: Bikepacking in the Nordics, Part 3
layout: post
permalink: /travelogues/:title/
excerpt: Bikepacking trip part 3
thumbnail: /assets/images/pxl_20230722_135813366.mp.jpg
classes: wide
---

“I have to admit that I haven’t made open fire” I told Julie, after my last exchange.

“It is not too difficult. And in the worst case, I have a stove”. Looks like at least someone came prepared. I was already contemplating my decisions. 

I helped to gather a few fallen wood sticks and twigs. We started talking, exchanged basic information, the typical stuff you do when you meet someone stranger. I came to know that she came from a small island from Germany as well. The big bike was hers and she was also bikepacking across Sweden and Denmark, back to her island. I was quite surprised that she was doing it solo being so young.

”It’s not that scary. And I have already done it once the last year”

“So do you use Komoot or something?”

“Nope, just going by the signs. I don’t even have a smartphone with me. Just an old Nokia and physical maps, just in case”

I was thoroughly surprised. On one hand it was me who had planned routes on Komoot, following on my bike computer to a dot, sharing my live location with my family, struggling to keep my smartphone battery charged. My bike was ultralight gravel bike with lightweight gear and neatly mounted saddle, frame, front bags. On the other hand it was Julie, going, quite literally, with the flow, with points of interest written in a big book, routes marked on a physical map and a bike which weighed more than everything I had.


We decided to try our luck with the quick lighter. Nope. Bear Gills made it look so effortless on Man Vs Wild, in reality, we spent more than 20 mins trying to light wood shavings with the quick lighter. Maybe it was too wet from the rain yesterday, but we couldn’t even produce half decent smoke. Thankfully Julie had a pocket lighter and some pieces of pine sap. That did the trick.


The sun was now dipping below horizon. Julie had big Aluminium utensils from her grandpa’s time which we used to make noodles. We talked about cities, food, music, people, life in general. It’s quite funny how two complete strangers can talk about anything just after an hour or two. Julie also prepped a tea from herbs she found in the wild. 

Sitting underneath the stars on a beach while warming ice cold hands in a crackling fire, those smokey yet tasty noodles, paired with sweet soreness in legs and finally a warm cup of herbal tea and endless talks. These are the evenings to bikepack for, to live for.

My sleep, however, not so nice. Apparently my inflatable camping mat had a hole, so I was basically sleeping on a cold hard ground. (Oh oh, trouble! TROUBLE!!) To make matters worse, my tent was on a slope. I wore my jacket and tried get some well needed rest.

I woke up the next morning with a sore back. But with the sound of chirping birds, crashing waves and an amazing panorama of the sea, I forgot about it pretty quickly. Julie was also, apparently, awake. She was also passing through Denmark and we shared the road until Helsingor. We made some coffee and then started on our way to the next stop, Helsingborg. But before that, I had wanted to fix my annoying mat. At the nearest harbor, we found one bucket and then subsequently, the hole. I just patched it with my quick tire puncture patch and hoped for the best.




When planning the route, I was surprised that my south Swedish coastal cycling route had only 500 m of elevation *in total* ! However, I could see that my bike computer, indicated a rather contrary reality—clocking over 400 meters on just the very first day.  

Rolling hills and valleys, the abrupt end of the mud trail, a patch of a busy highway, the crisp afternoon breeze along the coast, and an unexpected downpour—each element became a part of our journey. Undeterred, we trudged through it all.

-> More Pictures of the countryside 

As the sun started to set, we rolled in the cute town of Helsingborg. We grabbed a quick bite at some expensive burger shop and then looked around the town for postcards. Then we decided to look for camping places.

Surprisingly, it was super tricky. It was either the beach (without any wind protection) or somewhere in the ditch. We kept looking but couldn't find one. By the time, we had cycled again ~80 -90 kms and we were exhausted.

"Should we camp in the forest?" Julie asked. You are *technically* not supposed to camp in a natural reserve. But we were tired of finding other places, it was getting dark and crashing for the night was probably okay in a tiny part of the forest near Helsingborg. 

-> Jungle camp photos

Julie's stove came to the rescue today as campfire is a strict no! We cooked a bit of rice along with veggies. Put some salt, pepper, chilly and cheese in it. It was such a tasty meal!

-> grilling

It rained quite a bit in the night. But being in the woods, I didn't notice any winds. There was also some animal, probably a rat, tried to seek shelter inside my tent (no he didn't succeed)


We woke up quite late and made fruit salad in the morning for the breakfast. And ofcourse, some nice coffee. After writing letters, we wanted to post them. But we forgot it until very last minute and right at the ferry gate, we had to turn back to search for a postbox. 

-> tasty chocolate patiserry


-> Ferry crossing


After crossing the ferry, we entered Denmark! Julie had other plans of exploring the danish national forest, but I was already quite late with my plan. We promised each other to write letters and waved goodbye. I was on my own again. With 3 hours to sunset, 10% remaining battery, strong headwinds from the sea and another 70 or so kilometers to go, I was probably in trouble.




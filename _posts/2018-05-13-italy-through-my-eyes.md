---
title: "Italy Through My Eyes, Part 1"
layout: post
permalink: /travelogues/:title/
excerpt: "I am going to try avoiding the Tourist Guide-ish information. For travel itineraries and must-visit places, I followed google trips, lonely planet and Wikitravel. I’ll try to paint a picture as seen through my eyes. And of course, with some of the best captures from my Moto G5 Plus’ camera. "
classes: wide
header:
  image: /assets/images/travelogue/venice.jpg
  caption: "Venice, Italy | ©Shreyas Gokhale"
  teaser: /assets/images/travelogue/venice.jpg

author: Shreyas Gokhale
category: travelogue
layout: post
---

## ***“The Venetian Affair”***

When it comes to describing what is Venice, I get truly lost for words. The calmness of the canals, the winding streets taking you nowhere, and aroma of delectable Italian delicacies at every corner make you easily lose the track of time and space (and your smartphone battery!). This capital of northern Italy cannot be explored completely even in a month, forget about visiting in a day. For me, Italy is more than a tourist attraction. It was my first solo trip in Europe.

![](https://cdn-images-1.medium.com/max/8064/1*Z34kCiRrq4oFVFHtcLeZKA.jpeg)

I am going to try avoiding the Tourist Guide-ish information. For travel itineraries and must-visit places, I followed google trips, lonely planet and Wikitravel. I’ll try to paint a picture as seen through my eyes. And of course, with some of the best captures from my Moto G5 Plus’ camera. So let's begin!

![Rialto Bridgeside](https://cdn-images-1.medium.com/max/8064/1*EGW2rp0-tZr2AFP3qA2X3A.jpeg)

Everyone visiting Venice starts their journey from the north side of the city at Santa Lucia train/bus station as no vehicles are allowed in Venice down south. The entire city is a manifestation of classic architecture. As they say, Venice is a dream in stones. Walking on small cobbled paths makes you realize that your museum tour has already started. The city is riddled with baroque architecture. The Saint Mark’s square is often considered as a city center of Venice and you will always experience a stream of tourists going in either way. The square is filled with major attractions of Venice, one of which is the Doge’s Palace or Palazzo Ducale.

![Saint Mark’s Square](https://cdn-images-1.medium.com/max/8064/1*Y2N7GzaXcnTlLclnW9jZLw.jpeg)

Initially, I was mistaken that only ground floor was available for viewing which has pieces of historical architecture and sculptures. But boy was I wrong! The palace, which is an integral part of Venice, contains chambers having numerous paintings by popular artists, artifacts, and architectural marvels.

![Doge’s Palace from inside](https://cdn-images-1.medium.com/max/8064/1*wqmmrJED_S_bkCJWZHVgYQ.jpeg)

The famous pure golden staircase leads you to lavish rooms, including one which is the largest in Europe. Accompanying you will be the finest works of accomplished artists.

![Masterpieces on walls and ceilings in a chember](https://cdn-images-1.medium.com/max/8064/1*at7dnBE0J1CW0UzYBRK4_g.jpeg)

The palace is connected to the prisons via the popular ‘Bridge of Sighs’. After re-living the history for 2 and a half hours, I was still not able to get enough of this place.

![](https://cdn-images-1.medium.com/max/6048/1*zUWVHtj2EtAtGtXek9nHOA.jpeg)

![The Bridge of Sighs from outside (on left) and the view from inside the bridge (on right)!](https://cdn-images-1.medium.com/max/6048/1*6f11QsC19P2_vPMr5vicyA.jpeg)

The Saint Mark’s Basilica is not that impressive from inside but its terrace offers splendid views of Saint Mark’s square. You can take a sip of wine or take a bite of Italian Gelato while enjoying live music in the square.

![Saint Mark’s Basilica](https://cdn-images-1.medium.com/max/8064/1*DwzPDRIC2trVwnhqDGwV4w.jpeg)

After enjoying the mainstream attractions, do not forget to visit some of the hidden gems of Venice, e.g. San Pantalon church, which has a masterpiece painted on its ceiling. When it lights up, you can see its artistry and the depth effect.

![San Pantalon church. Unfortunately, no photos are allowed inside.](https://cdn-images-1.medium.com/max/6048/1*SaiDM6tK5a5KDijB1cSQHw.jpeg)

But there are so many amazing places in Venice that I am not sure if even a month’s stay will be enough!

Having lived in the Netherlands, the canals are not a novelty to me. But there is something magical about the canals and bridges of Venice. While walking through cobbled, narrow alleyways, you get a feeling of playing hide and seek with the canals. You lose when you end up at a point where you have to cross the canal, but there is no bridge. Retrace your path and start afresh!

![](https://cdn-images-1.medium.com/max/6048/1*TOn7mG9QfYsjJJFU1qL5bg.jpeg)

Sometimes during this game, you will reach a quiet, deserted alley. You will hear a faint noise of the bustle, the map will say that the streets filled with tourists are just a few meters away, but you will stay there, frozen in the time. At this very moment, you will realize that you have discovered the heart of Venice and also have won the game.

![Alleyways of Venice](https://cdn-images-1.medium.com/max/6048/1*zhSoAY7KYG8m1GNGW-BuHw.jpeg)

Generally, Venice is considered as the city of love. Indeed, you will see a lot of couples. Some even get married over here. Witnessing the sunset, they exchange their vows and then proceed to enjoy the gondola ride. Venetians often give a generous applause when a married couple comes over any bridge.

![](https://cdn-images-1.medium.com/max/6048/1*N4pJafxWZ5gF2UvXEcZTew.jpeg)

In Venice, you can easily travel on foot if you like walking. But when in Venice, do like Venetians do — ride Vaporettos (Water Buses). You can buy 1/2/3 or more days of Public transport pass which gives you free access to buses and Vaporettos. You can combine it with the museum card (not worthwhile in my opinion) and if you are a young student, you will get more discount! These Vaporettos ride in fixed routes and are not only convenient but also offer scenic views.

![A ride in a Vaporetto](https://cdn-images-1.medium.com/max/7540/1*1mAzC0UJ7GPZyQFzo7exyQ.jpeg)

Standing on the deck as the boat cruises through city canals, you will enjoy the majestic shoreline of Venice. As the sun sets, hearing splashing sound of water while watching the city glimmering in a golden yellow colour pass by will be a treat to your senses.

You don’t need me to tell you how amazing the Italian food is. When you get tired, enjoy a delicious meal in pizzerias of Venice. You will find these at every corner in the city. Big restaurants have live music to accompany the great food. For sad solo souls like me, there are a number of places where you can get takeaway pizza or pasta and enjoy it while sitting on a bridge. You cannot complete your dinner without having a bite of Italian Gelato. There are so many varieties to choose from! My favourite is ‘Bacio’ which literally means a kiss!

![Night in Venice](https://cdn-images-1.medium.com/max/8064/1*hJRpA3KY9CiCvVcsTi144Q.jpeg)

If dark is your cup of coffee (see what I did there ;) ), Italian Moka pot coffee will not disappoint you. You can also try hundreds of coffee varieties from baristos like Torrefazione Cannaregio.

After the sightseeing and having quick lunch, I started walking from S. Mark’s square towards the train station. As I bought souvenirs on the bustling street, I realized my Venetian affair is coming to an end. I don’t remember which one started first — the train of carriages or the train of thoughts. Crossing the Venetian Lagoon, I said Ciao to the charming city while promising myself to return soon. Now I was ready for my next adventure in not so popular, yet very beautiful part of Italy: Trieste!

![Crossing the lagoon](https://cdn-images-1.medium.com/max/8064/1*fqMZKCrc3gjjjrpKm7BpZw.jpeg)

### Stay tuned for Part 2!

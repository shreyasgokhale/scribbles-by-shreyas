---
title: "The Weekend Adventures, Part 2"
permalink: /travelogues/:title/
excerpt: "My adventures in France: Mount Saleve and Paris "
classes: wide
header:
  image: /assets/images/travelogue/saleve.jpg
  caption: "Mount Saleve, France | ©Shreyas Gokhale"
  teaser: /assets/images/travelogue/saleve.jpg
layout: post

---
## Mount Saleve and Paris

> *“I don’t think I can even lift my leg anymore”*

![](https://cdn-images-1.medium.com/max/6048/1*aZ6zQTNLpajb3EIryOU6ng.jpeg)

Right In front of the Tour Eiffel across the Seine, there is “Palais de Chaillot”. Like any other monument in Paris, it is simply magnificent! But its importance is compounded when you factor in its location: the gallery view of Paris! At around 8 AM in the morning, the place was just starting to flock with pigeons and tourists alike. Many newlywed were specifically there to photograph a perfect frame for their fairy tale: a backdrop of the rising sun behind the Eiffel! Paris is the city of love, even if you are single: you simply fall in love with the city! The weather was clear, the city was spirited and amidst this, I was sitting on the most uninteresting 26th or so stair to the top, clenching my left knee. It was hurting so much, that I was not even sure that I’ll be able to take my 2 PM train! How did this happen? Let me rewind it back 32 hours!

![Mount Saleve](https://cdn-images-1.medium.com/max/8064/1*LQ2YcLMkU33E3RE_A9irjw.jpeg)

It all started back in Geneva when I was planning my next day. You might remember from my last blog that I decided to trek on Mount Saleve, impromptu! **Mistake No 1:** NEVER trek without planning, no matter how much experienced you are. The trek’s description was “Medium” (which it is, for a regular alpine trekker!) The last time I trekked was 18 months before on the Harishchandragad Fort in the Sahyadris, India. Then “the Netherlands” happened and the only thing I climbed on was my bike! Still, the sweet thought of being able to trek again surpassed my initial hesitation and I set my alarm for 6.30 AM. After a quick breakfast, I hopped on a bus to the village of Veyrier, on the border of Switzerland and France. As it was an impromptu plan, I was wearing just my Canvas shoes, which was my **second mistake.** Not only they had a horrible grip but their flat sole made climbing painfully difficult. While enjoying the countryside from the bus, I saw the mountain getting bigger and daunting! I got down at the last stop and started to look around. I saw some people with trekking gears and asked them about the trek how I read something online and just came here to check out! As Geneva is in the French-speaking part of Switzerland, they didn’t understand a thing of what I was asking. I decided to follow my instincts (and a big group of people!)

![Telepherique Station and Saleve](https://cdn-images-1.medium.com/max/7572/1*3FsHLLvMuHf5ZQCrS_-U5Q.jpeg)

My instincts were right! Pretty soon I was a part of a large group who was led by some very experienced mountaineers. The most experienced was David, a 60 something but extremely fit leader. First, he asked everyone which language they felt comfortable in, and for the first time, I was able to spot a small minority group of English speakers. (We quickly became friends later on!) For us, he gave us a short brief about the Saleve and then an option: Either to climb via a scenic route in 5 hours or just follow him to the top on a difficult 3-hour route. No contest here, we all chose 1st one almost unanimously! Our small group of 10 was led by Monica: another seasoned trekker who climbed Saleve every Sunday for almost 15 years! We received some instructions from our leader, and then my first adventure in the Alps began!

![](https://cdn-images-1.medium.com/max/8064/1*RwU1elsblquNDdb7b7bt-A.jpeg)

![](https://cdn-images-1.medium.com/max/8064/1*GSR0YjrjP70RkB3cnnCvmg.jpeg)

![](https://cdn-images-1.medium.com/max/8064/1*nmvQxg7C0xVFWtvhnQb6bg.jpeg)

![Green Medows and Apple trees](https://cdn-images-1.medium.com/max/6048/1*Ms-aZ1TmODRlpd4_bnHBtQ.jpeg)

We started our trek from the bottom station of the cable car (Telepherique). With a steady pace, we started going along the mountain curve, gradually increasing the altitude. And slowly along with the course, the mother nature started unravelling its beauty. On our way to the top, we came across some breathtaking alpine landscapes.

![Amazing panoramic views](https://cdn-images-1.medium.com/max/8064/1*JoK4O1sTQrd64ChivdWlRA.jpeg)

![Our instructor Monica](https://cdn-images-1.medium.com/max/8064/1*Mf6TDQy4FcV_y_f964vddA.jpeg)

The view of lush Grass plains with an occasional glider landing on it, eating fallen apples and oranges from trees along the way, crossing streams of water and climbing rocky patches: all was simply picturesque. Panoramic cityscapes of Geneva always accompanied us, albeit changing their perspective as we went up! In my opinion, alpine trekking is strikingly similar to trekking in the Himalayas, but with better facilities and a bit lower difficulty.

![Plains on the top](https://cdn-images-1.medium.com/max/8064/1*Q0NRU_LGGFPFKs5o-hIoyg.jpeg)

![Views of other taller peaks in the Alps (There is Jungfrau in the back!)](https://cdn-images-1.medium.com/max/8064/1*FXeVVp4jNS2VaF2Mep_SZA.jpeg)

After 4.5 hours of arduous trekking, we reached the top. A spectacular view of nearby mountains and a serene Buddhist monastery: the perfect combination to rest your tired body and soul! That’s the reward that I was waiting for! My first alpine trek was a success. Unfortunately, my left leg was already paining a lot by this point.

![](https://cdn-images-1.medium.com/max/5712/1*5c8ag8kDZxwfg8PNgXOaDg.jpeg)

![](https://cdn-images-1.medium.com/max/8064/1*SOOivToL8-oRNYnh-1MJ9g.jpeg)

![](https://cdn-images-1.medium.com/max/8064/1*0xtxLmtLlXv1vubrvGU0yA.jpeg)

![Caves and panoramic views from the top. You can see Geneva down below!](https://cdn-images-1.medium.com/max/6048/1*R68s-FpbP3h729jYGcgVyQ.jpeg)

The shortest way to descend from there is to fly a glider! As we couldn’t choose this option, we all decided to descend the mountain by the Telepherique, i.e a cable car.

 <iframe src="https://medium.com/media/99ddebe90e290274ee7ed6afc3fc688e" frameborder=0></iframe>

![We walked along this!](https://cdn-images-1.medium.com/max/8064/1*OxAD-23yZJ9H6NtE8O0YzA.jpeg)

![](https://cdn-images-1.medium.com/max/8064/1*8VrLz9rn-y8dJxY2lWLvkg.jpeg)

![The landscape of Geneva while coming down the cable car](https://cdn-images-1.medium.com/max/8064/1*E8Z5eyOosEko6vYr2NHqSQ.jpeg)

On the bus back, I bid adieu to my new friends. I thought of resting a bit, but my sole was still hungry for more of Geneva. I went to the city to explore more. After roaming for a while, I boarded the bus to Paris at 11 in the night and prepared myself for the most annoying 7 hours of my trip.

![Daybreak in Paris](https://cdn-images-1.medium.com/max/8064/1*yKuL1Q31gd2JXJGykMtz0w.jpeg)

At around 5.30, the bus slipped into the sleeping city. The black canvas of the night was gradually getting painted with shades of amber. The bus stopped in a suburb at far south of Paris, reminding me of the terrible pain in my left leg. In response to my: “How to go to the Eiffel?” query, a nice lady from the bus literally held my hand and dropped me to the nearest metro station, explaining the route on the way! Being a Monday morning, Parisians were already rushing to catch their metro on time. The network is mostly underground but it pops out in the right places just so you can admire the beauty of Paris. The long stairs of stations (without elevators and escalators) quickly made me realize that this leg pain is not an ordinary one. I missed 4 consecutive metros, just while changing the lines! (Thank god that the metro runs every 3 minutes!)

![](https://cdn-images-1.medium.com/max/2000/1*PJVTA0dBvw8g_CAU9TDhuw.jpeg)

I trudged my way towards the big old rusty radio tower. The symbol of Paris, which was meant to be a temporary exhibit, was once loathed by Parisians for its ugliness! How ironic!

To see the iron lady from a better angle, I plodded towards the ‘Palais de Chaillot’, across the Seine. Walking a kilometre or so, I just crashed on the stairs.

This is the point where you contemplate about your life choices. Did I go a bit overboard with planning the solo trip? Definitely. Was it dangerous and painful to do all the activities at once?: Way over the line! Was it worth it? Totally! I kicked (!) back the pain and created a game plan to survive Paris.

Step 1: Get Some Medicine, ASAP
I googled and went to the pharmacy right in front of Eiffel. Thankfully they knew some English and gave me Ibuprofen, Diclofenac ointment and a crepe bandage. I could at least walk now.

Step 2: See all the monuments yet avoid walking.

![A: Tour Eiffel, B: Les Invalides, C: Pont Alexandre III, D: Louvre E: Place de la Concorde](https://cdn-images-1.medium.com/max/4144/1*H4t42RyB9F2ZkBOjx71odA.jpeg)

This was a difficult one and I hate exploring the city like window shopping. But sadly, there was no other option. Fortunately, I found a middle ground. I hopped on bus 89 and got down at the famous army museum. Walking through Les Invalides, I crossed the Seine on the famous Pont Alexandre III bridge. From there, I hopped on another bus to Louvre. As I didn’t have enough time to spend in the museum anyway, I simply ate crêpe and walked along. From Pont des Arts: Another famous bridge (where you can see the two islands and the mini statue of liberty) to the Louvre Pyramid, and then finally to the iconic Place de la Concorde, the walk turned out to be not so small!

![](https://cdn-images-1.medium.com/max/6048/1*UMNKT6yO6QAj3OlA7n1NuA.jpeg)

![Les Invalides (Left) Pont Alexandre III (Right)](https://cdn-images-1.medium.com/max/8064/1*df9qACYw89PfC1CE3nhTmg.jpeg)

![](https://cdn-images-1.medium.com/max/8064/1*ILghSvZdXWZ0jWKBeMcIcQ.jpeg)

![Lovre](https://cdn-images-1.medium.com/max/8064/1*MVNAMLYMwOjdlBMutmx69w.jpeg)

I walked almost 70 Km in 3 days, including 15km steep trek! My last stop was Gare du Nord Railway station. Exploring the remaining city through city buses, I realized that I had seen only a small fraction of Paris. I promised myself to return to Paris, with more days to spare. With tired body and soul, I boarded Thales High-Speed train to Brussels. The adventure was over, but my trip continued!

![](https://cdn-images-1.medium.com/max/6048/1*aQ895HzyXxXpnCxxDdRsgg.jpeg)

PS: Later I found out that I badly injured my left knee ligament and it needed 7 sessions of physiotherapy to recover. Such a trip is definitely not recommended, still totally worth it! :-p

---
title: Copenhagen
layout: post
permalink: /travelogues/:title/
excerpt: Solo trip to Copenhagen
thumbnail: https://lh3.googleusercontent.com/mBTw6fj8E9Nar8OGhnlwzrX5PjWMWO4ZlEN-0eIRLevvyGcGdYgA4rRXPAVUCixPFC5utS6c0KBH-zpVidGZ9w-sciZ6KJzCBwyzkwqToQkMe0GAf2-HUdxVEQZv5XYKnVmVAd1UtGA=w1920-h1080
classes: wide
---
It has been almost 1 year since the world was hit with the "C" word which completely changed our lives: how we work, eat, buy groceries, commute and even travel (or frankly, how we can't!). Although the infections have gone down all over the world, it feels quite bizarre to imagine a country without masks, lockdowns and restrictions. And yet, Denmark has managed to do exactly that. (Or at least it did, when I visited it in early September of 2021)!

Copenhagen is actually easy to reach by an 8 hour direct night train from Berlin. But, as I was doing just a weekend trip, I decided to take cough a bit of risk cough and decided to take a short 1 hour flight. Not so environmentally sustainable either, but after nearly a year of not flying anywhere, I was already in my flight mode. (And anyway, the big corporations should be much more responsible for slashing carbon emissions than guilt shaming individuals into buying bio carrots. Rant for some other time). With everything ready and travel playlist on, I ventured into the new world of post COVID-19 travel.

My flight was nice and smooth, although I did saw some huge windmill farms along the coast of Denmark

![]()

The new BER airport, which is infamous for 15 its year long delay, is quite big. Although a shame that the capital of Germany, does not have any outside EU flights apart from an occasional one bound for Istanbul.

![BER Airport](https://lh3.googleusercontent.com/bMBDmgB1Ts8XHzF64QtWCFeQCGWiuTO5zqtwWJRUtC3we1LFF44qavokJQDMqZMZ0kyRG6g8dPTbr-bEn6oKBsYZK16QZ6zrZyNmAudtIVut3ErW__1lg2kCnrfy3QtWYIvfnyHt_8I=w1920-h1080 "BER Airport")

The flight was barely 1 hour long, and I was greeted with nice German countryside, offshore windmills and finally the beautiful Danish port.

A quick glance on my vaccination pass from the authorities and I was off to *København*. As I exited the airport, I finally saw how pre-COVID world felt like. No masks, no restrictions, even inside buildings and supermarkets. Some people even stared at me as I bought groceries wearing one. Soon enough, I got acquainted to the olden days!

Although I would pick bike over public transport in an instant, my previous research told me to rely on the amazing metro in Copenhagen and I bought [a 48 hour, small city pass](https://dinoffentligetransport.dk/citypass), adequate for my stay and also the trips to the airport!

![](https://lh3.googleusercontent.com/pw/AM-JKLWSj8fUmdSeklLBBtSNLdbuTw784YArz9ArWnK2_9uiI-WtCFQ7K-uaf4O6k-92N7aC83JvbneHoXqrLe2JgwF1Glzhn-HzmJXcSsY-F3Hh6QIw0xy-5jTOmg5qR6aEV5goBSnXlsUUUaQMGGGip_3E8w=w978-h1304-no?authuser=0 "Mojo-Jojo Lair / Tycho Brahe Planetarium")

The place where I crashed, "Steel house Copenhagen", is located on the opposite side of of the city center, but not that far from any action. 10 mins of walk from the central station and I was in. Nothing special about the rooms but the lobby was amazing. They even had a movie theater, a small concert stage and a pool! In Danish, there is a unique word to describe something warm or cozy: hooghly. The main lobby was soo hooghly that I wanted to just plump down in sofa and read books the whole day.

![Hoogly Hostel](https://lh3.googleusercontent.com/pw/AM-JKLV-7B4JCEwvdejH9S5gSFaHvPuw3u7ihic4Y7obdIKL9l9g2dFChi8iR1dkoVsIhWWMXSgxDMj84jOahu5_FDZvnowziVMJb3rqt7A2-x5nZJMJuh-Rqbt-Z0_b6NTi1ajSleu2NIRJFk1eUA2HkF2oEA=w1306-h979-no?authuser=0 "Hoogly Hostel")

When I am in a new city, I have tried and tested process of exploring it. The very first thing I do after dumping my luggage is just get out and start wandering aimlessly. Just open maps and see where the city center is, no need to mark places. This way you are almost guaranteed to stumble upon some of the highlights of the city serendipitously. Even if you don't, you will get to know the 'vibe' of the city. 

Coming from Berlin, Copenhagen felt crazy expensive (Almost as expensive as Switzerland)! These guys also opted out of euro and have their own Danish Krone, but throughout my trip, I did not see not even 1 cent of physical money: everything digital. Germany should take notes (or they really shouldn't ;) ). 

![](https://lh3.googleusercontent.com/pw/AM-JKLXmGHLwlGMF7nu-LST7qyl9NYZZoIGFNW2IkvbFWT0L_iCq9Oof3J9Tc1rD0mA7LPS3pncv43OHOnWDdpaW3U5Um0zXWDy_bdp7sTTzKqFx6ZTwXcFD9sPm1bOTf4w02MRY1fvSU7qCIKJLt4pB4IINpg=w2030-h1970-no?authuser=0 "Denmark is full of 7-11s which have this amazing long lost cousin of South Indian Paddu / Appam.")

![Nyhavn at night](https://lh3.googleusercontent.com/pw/AM-JKLUHQlA7oxDaSHm_OXgXegNfhplaC-6-Vjdl-TcM2uLcvqYnvN4Prwlrgwtj_Ur8Am24rLAqHMfociEMWTFRlEx_yYQsaCrs4-ex7H3cJvQtEY4QspDseyY3DsSyqVGqYUoKtlT--879vZJ9cVMproR7qg=w2624-h1968-no?authuser=0 "Nyhavn / New Harbor at night")

Wandering around, I eventually reached the famous Nyhavn area. It looked exactly as the pictures! Eating Chocolate Churros while sitting on the docks under the vivid neon signs reminded me that you can find peace even in the middle of chaos. 

Next step in our city exploration starts at day 2: A guided walking tour. Such tours last 2-3 hours,  are guided by passionate bilinguals or locals and are usually tip based. Funny, witty, insightful: experience differs from tour to tour but they are amazing ways to connect with fellow travellers, especially in a solo trip. I already booked one with copenhagenfreewalkingtours.dk a grand walking tour at 10.30 on Saturday. 

Saturday morning was typical Danish: cold and rainy. After another short round of aimless wandering (this time around the Citadel gardens), it was now the time to look for the red umbrella of free walking tours.

![Citadel](https://lh3.googleusercontent.com/pw/AM-JKLUZ9CYUC4Al3zMyRZj-gsPlstTJN7HAw6bZXu67eBiLOR_S2Dlp4U117G5HGKZ4Z5gZCFraUuOWlh1-0yDUZagJ2moIvqkFcUO2m_C9AKqCQQwhPy1X5WcZTNs3HRod2u_oVLnOKEZL_BSlm6lBAF0UCA=w2624-h1968-no?authuser=0 "Copenhagen Citadel")

### Free walking tour

The tour started from one of the most iconic spots in Copenhagen: *Rådhuspladsen* or city hall square.  The one thing I got my attention was "[The Weather Girl on the Bike (Barometer)](http://www.copenhagencyclechic.com/2007/12/cycle-girl-has-been-somewhat-of.html)" and other weather statues explaining the current weather (which, guessing Copenhagen, needs no explanation). Our quirky and humorous tour guide, while enlightening us with history lessons also took us through the winding streets of the old city. 

![City Hall](https://lh3.googleusercontent.com/pw/AM-JKLWW_RVjdI1gM_-Wpm4HQr4Omjs6JS-Tvaz63aswqsi6rRKr-2t5U7yqiU_rPJuGKKcPIYgN0D0-ArxN27nqFKC-NUg_VeGeBp-WaIVjqgLu7P9yC3GrIzmgTSXXtPAnw3eKHKwtnisO2dYATTob91IaAw=w2624-h1968-no?authuser=0 "City Hall")

Copenhagen's tale is riddled with wars, Vikings and the Swedes. The "*Havn*" or the harbor was a major outpost of Vikings. They carried out numerous raids across the Europe which put Denmark and in turn Copenhagen on the map. Through many kings, kingdoms and wars with Sweden later, Copenhagen maintained it's position of an influential port capital. In WW2, Hitler took over the city but the fearless Danes helped countless Jews to escape to Sweden. 

![](https://lh3.googleusercontent.com/pw/AM-JKLUbBs_g38CtmtGobs2WVt7k7aEJAzjvRf6-XiM689bTWMA7GDmF2nfwKit9Dwe4xYcYUwyDrDsQqsEpqGNjjDlTx4nUfZtU9X0IpBptWrGG1kdDA0vaHlH2-pvl89ptBEw13hoh8EeFtssk5qSFIhOJeg=w1306-h980-no?authuser=0)

![Amalienborg](https://lh3.googleusercontent.com/pw/AM-JKLVI43pqewV65Jko_tCDCYCUsqqbyMu_GHJmmzFQkC1C27-0a7bZrjGgFpQkl2fILlIWdF9wjY2gQFOtO7N0Rho-tcDKVcdeCn1lqGWsBl9IuGesvMfxNrV9Ig0KN9XOmZlZ3XK5iB-y-fzXcMYpJaWjSA=w1306-h980-no?authuser=0 "Amalienborg")

We were also ambushed by a unique Victorian bicycling parade (!) and Yearly Copenhagen marathon.

![](https://lh3.googleusercontent.com/pw/AM-JKLWyIL8DfCJOzVYInDRp7su7koxgV5o33Dklx2dERiPPLXJmf-3G_wxyzoYxuuxU4vnZprOkRLkQgzZdhPPIo72M0jLfPyJCCVHKNfAYsXWDPAmAP1Pve4yGwtXUhjxYd0Czh_L4c6ZYtTJw6wLfJrlmlg=w2624-h1968-no?authuser=0 "Victorian Bike Ride!")

![](https://lh3.googleusercontent.com/pw/AM-JKLU0SFMLnV1VTGfKWpVMVxlK3xbRl3bKQWPNJ0Ijil5HkwzWW3MtwBqdLgtWJxRjXbTXv3ru9Z-HLEpXiUxHTtX9EzWDNGgJchIXYmLN6AFwtBa8-GH2zxybkTTvJsh-2UZAfOVFY8-ddj5ZfNNs6yvhgg=w735-h980-no?authuser=0)

### The boat tour

By the time we reached the Nyhaven, it was already well past lunchtime. Our 3 hour tour was over even before we realized it started. I, along with 3 others from the group, crossed the *Inderhavnsbroen* footbridge and head towards the food court. While chatting (and eating delicious Jackfruit vegan burritos) we realized that a boat ride through the canals is a must to explore the port city to it's fullest. And so that's exactly what we did!

![Boat ride and super tight bridges](https://lh3.googleusercontent.com/pw/AM-JKLV_7yMlY6oPdEwI-Hj9k1rP3nftih49l8XLUhF1EdcU9yHsw70wME0QZL5sddTd7f1His72-tHyFEIMCsRKTECI2l7v_ERbgPy9LcemRXiihe4QRK8ZWzkjuYYiM4cAHVqChrgcXi3pOvj2yUOz5G55iA=w1213-h980-no?authuser=0 "Boat ride and super tight bridges")

![](https://lh3.googleusercontent.com/pw/AM-JKLWc8WkvBknMgqYKw8D0DDO5BmED7IKj846zBCKnlPKaGE8ZtjWUm-MgtcN0wN3c2GUDtehIDpt4XSsqyWu4R5Q-UvHiNrxj60u5AUa9qnes6xouskkNwjwRZOYJjG2LvsGP1IOiomTP6ybyH-Ndi6xS1w=w1306-h980-no?authuser=0)

![Børsen](https://lh3.googleusercontent.com/pw/AM-JKLW11WoNWrh3s6AGxxe4HtzwKbD0LZEXbpu7nHTv0-5uRtEwdi5xlDBlKtdboLL9FXeR3w50oIkdiMIgPFcHilm2AHHTOMwq7f-QrLY1uydo1kYk4ZuajCtFU7mLcXSCUfDyosUp4YxctWrGdXy2FJC4eQ=w735-h980-no?authuser=0 "Børsen, 17th century stock exchange")

#### Christiania

"The free country of Christiania".

I had never heard of this Before making my itinerary of Denmark, and it was on the top of most "To-Do" lists, so I decided to see for myself what the fuss is about. Christiania is an anomaly in Copenhagen. Photography and running are strictly forbidden. What I saw was a collage of graffiti on dilapidated buildings and a very "lax" culture. 



For some reason, this gave me vibes from a very particular area in Berlin. People were roaming, talking loudly, smoking and having an overall chill time. Although there were some cops, they were mostly overlooking what people were doing. For an otherwise very organized and immaculate Danish person, this must be a place to be free. It's funny how people will have 2 version of themselves, one which works 9-5 and lives by the rules, only to spend the weekend in a crazy wild club with a completely different persona.
---
title: Solo Bikepacking in the Nordics, Part 2
layout: post
permalink: /travelogues/:title/
excerpt: Solo Bikepacking adventures in the Nordics, Part 2. From Trelleborg to Malmo
thumbnail: /assets/images/pxl_20230720_103238492.jpg
classes: wide
---
It took painfully long 4 hours to reach Rostock with the snail like pace of Regional Express. I realized it a bit late that I also have to bike for 40 mins to reach the port from the train station. Fortunately I had some buffer time in between. I scoured through. Reaching port at 21.00, just in time.

![Rostock](/assets/images/original_053964e6-37c8-4c54-980f-d65812f51d80_pxl_20230719_182302042-edit.jpg "Rostock")

## The cruise ship

My cruise ship was Stena line, taking 7 hours overnight to reach the port of Trelleborg. I saw fellow bikepackers waiting for boarding at front of the car line. The bicycle storage was free, but we had to find our way out through huge cargo trucks. To have a proper rest before the adventure, I booked myself an expensive sleeping pod. Although, I quickly realized that it was quite unnecessary. People were just using sleeping bags to sleep on the floor! Nevertheless, I slept like a baby.

![Ship deck](/assets/images/pxl_20230719_194331850.mp.jpg "Ship deck")

![View from cabin](/assets/images/pxl_20230720_030346261.jpg "View from cabin")

I don’t know if you have felt this: when you are at a completely new place, your brain becomes super aware of the surroundings. Evolution? Maybe.. But it starts collecting and processing the data recorded by the senses at a rapid place. When the ship pulled in the sleepy port of Trelleborg, the same thing happened with me. Clear, bright blue skies. Oh wow, it looks different. Beautiful! So calm. Cold. COLD! It smells incredible!

It indeed smelled like an exquisite bottle of home fragrance for the entire length of the Sweden trip. 

![Trelleborg](/assets/images/pxl_20230720_040945683.jpg "Trelleborg")

## Trelleborg

Trelleborg is a well known and ancient port. I hadn’t eaten anything for the last 10 hours and so famished. But what would be open this wary hour? I looked on google maps and found that the oldest bakery in Trelleborg is open! I had to pay a visit. 

Cafes and bakery in Sweden is an another affair, intertwined with the afternoon coffee break: Fika! A welcoming host, a cozy table and a chair, books - magazines to read and unlimited coffee (yes, unlimited refill!) What else do you need. My host greeted me warmly, also offered me a place to park my bike. 

![Super cozy cafe](/assets/images/pxl_20230720_045814505.jpg "Super cozy cafe")

![Super cozy cafe](/assets/images/pxl_20230720_042356009.jpg "Super cozy cafe")

I bought Kanelbulle, a traditional cinnamon roll and, of course, a coffee.

![Kanelbulle](/assets/images/pxl_20230720_060732946.jpg "Kanelbulle")

As I drank my second cup, my rough plan for the day become clear. I would go to Malmo for lunch, and sleep somewhere in between Malmo and Landskrona. Maybe somewhere around a shelter? Lets see. 

## Malmo

Cycling to malmo was paradise. Fields of gold and a red Swedish barn at a distance. It couldn’t get more idyllic. Swedish countryside was empty, I hardly saw anyone. By the time I stated noticing some traffic, I realized that I had already cycled 40 KMs and have arrived in Malmo! 

![Going to Malmo](/assets/images/pxl_20230720_053220094.jpg "Going to Malmo")

Malmo is the 3rd largest city in Sweden, but is more like a little brother to the neighboring Copenhagen. Both are joined by a huge bridge over the Oresund strait. You can easily see this engineering marvel from the either side of the strait, even until Helsingborg. 

![](/assets/images/malmo-city.jpg)

![](/assets/images/pxl_20230720_094704270.mp.jpg)

I ate quick lunch in Malmo and had a realization of just how expensive Sweden is! A small toast and a dessert for 14 euros! But the cafe stop at least allowed me to charge my depleted phone battery.

![](/assets/images/sea-and-road.jpg)

After lunch, my pace slowed down a bit. The sun was beaming down intense heat and I was stopping every now and then to fill my water bottles at WC. The Swedish WC infrastructure is one of the best of its kind. In Malmo, there are hundreds of free public toilets. Each toilet is quipped with multiple rooms, including wheelchair accessible ones, showers, water spouts and even air pumps. 

![](/assets/images/wc.jpg)

I rode almost 80KMs and decided to call it a day. I was near an amazingly beautiful port of [Barsebackhamm](https://en.wikipedia.org/wiki/Barseb%C3%A4ckshamn). In Sweden (and many other European countries), there is a concept of shelters for hikers. If you need a place to stay dry during a rain, or to crash the night, shelters are perfect. I thought that it would be a good place to camp, so I looked for nearby shelters on the shelter app. There are some very interesting shelters in Sweden, built by [innovative group of architects](https://www.skaneleden.se/item/fler-arkitektritade-vindskydd-klara-pa-skaneleden/wo4xojtelbegy0pexsgv904xcysuoqop). And I saw one from this nearby, so decided to have a closer look. 

The shelter was in the shape of a bird’s nest, overlooking the sea. What a beautiful design with an amazing view! I saw a father and his son camping nearby and a parked old bike near the shelter. I waved at the father and walked a bit further to take a good look at the shelter. 

![](/assets/images/camp.jpg)

”Hey there!”

I heard someone calling me from the shelter. A young girl with rainbow hair climbed down the stairs.

”Hello Hello!”, I replied

“Are you going to stay here?” She asked. 

“Yes, I was planning to camp here. Are you going to stay in the shelter?”

“Yes, but it is quite big. You can also use it”

“No No, I was planning to camp” I replied

“Are you sure, I don’t really mind”

“Yes I am, I really wanted to camp” I really wanted to have the camping experience. 

“Okay! I am going to make fire”

Ah yes, food. I was super hungry. Unfortunately, due to low space and empty decathlon shelves, I didn’t pack the stove with me. My best choice was to somehow conjure fire with a Swedish lighter. 

“Here?” I asked

“Yes, with firewood” She replied

“Do you need help? Maybe we can join forces”

“Sure!” She replied.

That’s how I met Julie.


#### Track Day 1: Trelleborg to Landskrona

{% include youtube.html id='dPiNJ2R4TJE' %}
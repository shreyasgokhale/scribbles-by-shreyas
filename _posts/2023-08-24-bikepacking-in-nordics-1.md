---
title: Solo Bikepacking in the Nordics, Part 1
layout: post
permalink: /travelogues/:title/
excerpt: >
  When I was in the 6th grade, my Baba had a calendar from a company named
  Sandvik. It featured pictures taken in that month from different parts of
  Sweden, where the company is based in. A guy in blue jacket snowboarding in
  knee deep snow for February, cozy wooden houses with smoke coming out of their
  chimneys for November, long and calm beaches with red-white striped
  lighthouses for one of the Summer months and so on. On every first day the
  months, I used to be filled with excitement to turn the leaf and gaze at the
  new picture. You can imagine my despair when I found out that Sandvik decided
  to go with pictures of machines for their next installment of the calendar. 


  I usually forget things and events that have happened in the past, but this memory, I remember it clear as a day. And ever since that year, I had a yearning to go to Sweden. As a kid who had never crossed a state boundary until then, I never would have imagined that I will ever be able to visit Sweden, let alone bike solo and camp on one such beach with red-white striped lighthouse.
thumbnail: /assets/images/img_0049.jpeg
classes: wide
---
When I was in the 6th grade, my Baba had a calendar from a company named Sandvik. It featured pictures taken in that month from different parts of Sweden, where the company is based in. A guy in blue jacket snowboarding in knee deep snow for February, cozy wooden houses with smoke coming out of their chimneys for November, long and calm beaches with red-white striped lighthouses for one of the Summer months and so on. On every first day the months, I used to be filled with excitement to turn the leaf and gaze at the new picture. You can imagine my despair when I found out that Sandvik decided to go with pictures of machines for their next installment of the calendar. 

I usually forget things and events that have happened in the past, but this memory, I remember it clear as a day. And ever since that year, I had a yearning to go to Sweden. As a kid who had never crossed a state boundary until then, I never would have imagined that I will ever be able to visit Sweden, let alone bike solo and camp on one such beach with red-white striped lighthouse.

## Bikepacking and me

If you have been following [@bike.around.berlin](https://www.instagram.com/bike.around.berlin/), you know that I like to bike (or cycle or bicycle). When my trusty, second hand roadbike: Giant OCR 2 got stolen in November 2022, I decided to go for a gravel bike for my next one. One reason of this was to explore numerous gravel trails around Berlin, but also to dabble into bikepacking. There is something about taking all of your belongings, putting it on a bike and exploring far and wide. Cannondale Topstone 3 was the perfect bike for me. Collecting gear one by one, I did the first 2 day long solo bikepacking during Easter of 2023: from Berlin to Wittenberg and then to Magdeburg and an another one in May 2023: in Lower Saxony through Harz mountains. Although these two trips were very adventurous in their own ways (I had a bad puncture without supplies in basically a forest and had to be rescued by a taxi!, story for another day), I always stayed in a hotel for the night, never camped.

> Bikepacking in Wittenberg
> ![Bikepacking in Wittenberg](/assets/images/bikepacking-1-wittenberg.jpg "Bikepacking in Wittenberg")
>
> Bikepacking in Harz
> ![Bikepacking in Harz](/assets/images/bikepacking-1-harz.jpg "Bikepacking in Harz")

Staying somewhere from scratch adds another thrill to it. You have to carry your house (tent) with you, along with some food, cooking etc. But it also makes you free, not limited to a location, an itinerary, an obligation to reach somewhere for the night. At least in theory. Unfortunately, it is not technically possible to wild camp in Germany, you have to stay at campsites (I’ve heard that bikepacking might count as hiking, which is an exception, but why risk it?)  Which means you have to prebook, especially in the summer months.

But here comes the best part: you *can* wild camp in Sweden! So my brain went 2+2 = 4 and that’s how I started planing a week long bikepacking in the Nordics. 

## The prep

Taking the days off was the easiest part. I had some camping experience in the Himalayas, but that was with a preplanned group. I didn’t even set up my tent before. Sweden was an unknown environment. Mostly safe from wildlife perspective. But different language, laws, lifestyle and, in general, *feeling*! I decided that I need to familiarize myself with things as much as I can, for the rest I will follow Improvise, Adapt, Overcome. 

![](/assets/images/improvise.jpeg)



I started with a list of things that I wanted to buy: tent obviously, but also small things. I had already asked everyone I know if they wanted to join me, but I know how difficult it would be to adjust all the schedules, so I was prepared to go alone. Next, I started charting my route.

Sweden is quite a huge country, so I focused on the south. Skane county is famous for it’s beaches and cute towns and the [Sydkustleden](https://sydostleden-sydkustleden.se/en) cycling route. The route is part of bigger [Eurovelo route 10](https://en.eurovelo.com/ev10) and is very well maintained. What more, Trelleborg has a direct ferry connection from Rostock, which is directly connected via RE5 from Berlin. With 49 euro Deutschlandticket, this also fit my budget very well. I mapped up a route on Komoot, starting from Trelleborg and going to Malmo, Landskorna and Helsingborg. Then, crossing over to Denmark in a ferry Helsingor, the trail went down south until Copenhagen. From Copenhagen, going over oresund bridge to Malmo again and then going to Skanor and back to Trelleborg. My plan was complete.

<iframe src="https://www.komoot.com/collection/2203918/embed" width="100%" height="580" frameborder="0" scrolling="no"></iframe>

## Trial run

Before jumping in the ocean directly, it is better to wet the feet a bit. I decided to go on 2 overnighters before the big one. For the first one, I went to a camp in Erkner after work. With my newly bought Robens Arrowhead 1P tent, I gathered all the supplies that I *thought* I needed. Setting up camp took 30 mins, but I immediately notices that I straight up lacked some necessary stuff such as a good pillow and some good to have stuff: like an ultra-lightweight sleeping bag. I wanted to do another overnighter to finally test all of my gear, but I had to cancel that because of no time. Instead, I kept doing repeated trips to Decathlon until the very last moment. I even 3D printed a few carbines! My final setup looked something like this:

> Ready to go
> ![](/assets/images/bikepacking-1-setup.jpg)

The bike + luggage load totaled to be at 23 kgs, which I was super proud of (even my bikestore guy was surprised!) After finishing work on Wednesday 19 July, I clipped in my shoes, took a big breath and peddled my way to Berlin Potsdamer Platz (to avoid crowds at the Central Station!) for my first ever, true bikepacking trip.